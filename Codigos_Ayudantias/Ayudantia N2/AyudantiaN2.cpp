#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

void GammaCorrection(Mat& src, Mat& dst, float fGamma)
{
	unsigned char lut[256];
	for (int i = 0; i < 256; i++)
	{
		lut[i] = saturate_cast<uchar>(pow((float)(i / 255.0), fGamma) * 255.0f);
	}
	dst = src.clone();

	const int channels = dst.channels();
	switch (channels) {
	case 1: {
		MatIterator_<uchar> it, end;
		for (it = dst.begin<uchar>(), end = dst.end<uchar>(); it != end; it++)
			*it = lut[(*it)];
		break;
	}
	case 3: {
		MatIterator_<Vec3b> it, end;
		for (it = dst.begin<Vec3b>(), end = dst.end<Vec3b>(); it != end; it++)
		{
			(*it)[0] = lut[((*it)[0])];
			(*it)[1] = lut[((*it)[1])];
			(*it)[2] = lut[((*it)[2])];
		}
		break;
	}
	}
}

int main(int argc, char** argv){

	Mat image = imread("D:\\Documents\\Python_Scripts\\Taller_USM_Ed.Futuro\\Imgs\\Image07.jpg");
	// Mat image(600, 800, CV_8UC3, Scalar(0, 0, 255));

	int cols = image.cols, rows = image.rows;
	resize(image, image, Size(cols/ 6, rows / 6));

	Mat rgb, hsv, gray, yuv, Y, bit, gamma;

	cvtColor(image, rgb, COLOR_BGR2RGB);
	cvtColor(image, hsv, COLOR_BGR2HSV);
	cvtColor(image, gray, COLOR_BGR2GRAY);
	cvtColor(image, yuv, COLOR_BGR2YUV);

	vector<Mat> planes;
	split(image, planes);
	imshow("Blue", planes[0]);
	imshow("Green", planes[1]);
	imshow("Red", planes[2]);

	Mat out;
	merge(planes, out);
	imshow("Out", out);

	imshow("Original", image);
	imshow("RGB", rgb);
	imshow("HSV", hsv);
	imshow("Gray", gray);
	imshow("YUV", yuv);

	inRange(hsv, Scalar(140, 50, 50), Scalar(180, 255, 255),Y);
	bitwise_or(image, image, bit, Y);
	imshow("Bitwise", bit);

	GammaCorrection(image, gamma, 0.2);
	imshow("Gamma", gamma);

	waitKey(0);

	destroyAllWindows();

	return 0;
}