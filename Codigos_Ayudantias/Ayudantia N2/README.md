## Ayudantia N2

Ayudantia realizada el 20 de Septiembre de 2021.
En esta Ayudantia se estudiaran los siguientes temas:

 - Cargar y mostrar una Imagen
 - Obtencios de algunos parametros
 - Cambiar tamano
 - Conversion de Color a **RGB**, **HSV**, **YUV**, **GRAYSCALE**
 - Separacion de Canales
 - Obtencion de rangos de colores
 - Correccion Gamma