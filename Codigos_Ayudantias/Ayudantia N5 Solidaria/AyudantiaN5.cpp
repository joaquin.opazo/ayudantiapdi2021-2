#include <iostream>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

void funcion(Mat I, int a, int b) {
	Mat YUV;
	vector<Mat> planes;
	cvtColor(I, YUV, COLOR_BGR2YCrCb);

	split(YUV, planes);

	uchar *data = planes[0].data;

	for (int i = 0; i < I.cols * I.rows; ++i){
		if (data[i] < a) {
			data[i] = 128;
		}
		else if (data[i] < b) {
			data[i] = (data[i] * (255 - b) + 128 * b - 255 * a) / (128 - a);
		}
		else {
			data[i] = 255;
		}
	}

	merge(planes, YUV);
	cvtColor(YUV, I, COLOR_YCrCb2BGR);
	imshow("Funcion Aplicada", I);
}

void stats(Mat YUV) {
	int max = YUV.rows * YUV.cols * 3, count = 0;
	float AverageU = 0.0, AverageV = 0.0;
	uchar *data = YUV.data;

	for (int i = 0; i < max; i += 3) {
		if (data[i] >= 100 && data[i] <= 200) {
			count++;
			AverageU += data[i + 1];
			AverageV += data[i + 2];
		}
	}

	AverageU /= count;
	AverageV /= count;

	cout << "Cantidad de Pixeles con luminosidad razonable: " << count << endl;
	cout << "Crominancia Promedio: " << endl << "\tU: " << AverageU << "\tV: " << AverageV << endl;
}

int main(int argc, char *argv[]){
	/* Ejercicio 01
	Mat src;

	string filename = argv[1];
	src = imread(filename, 1);
	resize(src, src, Size(600, 600));
	imshow("Original",src);
	
	int a, b;
	a = atoi(argv[2]);
	b = atoi(argv[3]);

	funcion(src, a, b);

	waitKey(0);
	destroyAllWindows();
	*/

	// Ejercicio 03
	Mat src,YUV;

	string filename = argv[1];
	src = imread(filename, 1);
	resize(src,src , Size(500,500));
	imshow("Original", src);
	cvtColor(src, YUV, COLOR_BGR2YCrCb);
	stats(YUV);
	waitKey(0);
	
	return 0;
}
