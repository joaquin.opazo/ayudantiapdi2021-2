## Ayudantia N3

Ayudantia realizada el 27 de Septiembre de 2021.
En esta Ayudantia se estudiaran los siguientes temas:

 - Filtros Suavizados como **Gaussian** y **Mediana**
 - Detectores de Borde como **Canny** y **Sobel**
 - Thresholding