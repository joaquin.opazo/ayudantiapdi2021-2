#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char** argv){

    Mat image = imread("D:\\Documents\\Python_Scripts\\Taller_USM_Ed.Futuro\\Imgs\\Image07.jpg");

    int cols = image.cols, rows = image.rows;
    resize(image, image, Size(cols / 5, rows / 5));

    Mat gray;

    cvtColor(image, gray, COLOR_BGR2GRAY);

    Mat Gaussian, Median, boxfilter;

    GaussianBlur(image, Gaussian, Size(3, 3),BORDER_DEFAULT);
    /* Types:
        BORDER_CONSTANT
        BORDER_REPLICATE
        BORDER_REFLECT
        BORDER_REFLECT_101
        BORDER_TRANSAPARENT
        BORDER_DEFAULT
    */

    medianBlur(image, Median, 3);

    Mat sobel,canny;

    Sobel(Gaussian, sobel, -1, 1, 1);
    Canny(Gaussian, canny, 100, 150);

    Mat TH;

    threshold(gray, TH, 125, 255, 1);
    /* Types:
      0: Binario
      1: Binario Invertido
      2: Truncado
      3: A 0
      4: A 0 invertido
    */

    imshow("Gaussian", Gaussian);
    imshow("Median", Median);
    imshow("Sobel", sobel);
    imshow("canny", canny);
    imshow("Threshold", TH);

    waitKey(0);
    destroyAllWindows();
}

