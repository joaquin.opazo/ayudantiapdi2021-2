#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs/imgcodecs.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/video/background_segm.hpp"
#include "opencv2/videoio/videoio.hpp"
#include "opencv2/core/core.hpp"
#include <iostream>
#include <sstream>

using namespace cv;
using namespace std;

int main(int argc, char** argv) {
	
	VideoCapture cap(0);
	Mat hsv, frame;

	while (true) {
		cap >> frame;

		cvtColor(frame, frame, COLOR_BGR2HSV);
		inRange(frame, Scalar(5, 50, 50), Scalar(25, 255, 255), hsv); //Orange
		

		erode(hsv, hsv, getStructuringElement(MORPH_RECT, Size(7, 7)));
		//dilate(hsv, hsv, getStructuringElement(MORPH_RECT, Size(7, 7)));

		imshow("Hsv", hsv);

		if (waitKey(10) == 27) {
			break;
		}
	}

	return 0;
}